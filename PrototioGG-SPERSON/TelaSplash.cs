﻿using PrototioGG_SPERSON.Telas.Logar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrototioGG_SPERSON
{
    public partial class TelaSplash : Form
    {
        public TelaSplash()
        {
            InitializeComponent();

            
            Task.Factory.StartNew(() =>
            {
                
                System.Threading.Thread.Sleep(5000);

                Invoke(new Action(() =>
                {
                    // Abre a tela Inicial
                    FrmUsuarioLogar frm = new FrmUsuarioLogar();
                    frm.Show();
                    Hide();
                }));
            });

        }



    }
    
}
