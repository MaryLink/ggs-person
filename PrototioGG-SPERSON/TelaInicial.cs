﻿using PrototioGG_SPERSON.Classes.Funcionario.BaterPonto;
using PrototioGG_SPERSON.DB.Funcionario;
using PrototioGG_SPERSON.Telas.Cadastrar;
using PrototioGG_SPERSON.Telas.Consultar;
using PrototioGG_SPERSON.Telas.Logar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrototioGG_SPERSON
{
    public partial class TelaInicial : Form
    {
        public TelaInicial()
        {
            InitializeComponent();
            if (SessãoUsuario.UsuarioLogado.FK_Departamento != 1)
            {

                tsmDepartCad.Enabled = false;
                tsmFornecedorCad.Enabled = false;
                tsmFuncCad.Enabled = false;
            }



    }

        public void CarregarTela()
        {
            panelCentroinicial.Controls.Clear();

            if (panelCentroinicial.Controls.Count > 5)
            {
                panelCentroinicial.Controls.RemoveAt(panelCentroinicial.Controls.Count - 1);

            }

        }

        private void funcionarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            FrmFuncionarioCadastro tela = new FrmFuncionarioCadastro();
            CarregarTela();          
            panelCentroinicial.Controls.Add(tela);

        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
    
            FrmClienteCadastro tela = new FrmClienteCadastro();
            CarregarTela();
            panelCentroinicial.Controls.Add(tela);

        }

        private void produtoToolStripMenuItem_Click(object sender, EventArgs e)
        {
     
            FrmProdutoCadastro tela = new FrmProdutoCadastro();
            CarregarTela();
            panelCentroinicial.Controls.Add(tela);
                

        }

        private void fornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
            FrmFornecedorCadastro tela = new FrmFornecedorCadastro();
            CarregarTela();
            panelCentroinicial.Controls.Add(tela);

        }

        private void funcionarioToolStripMenuItem1_Click(object sender, EventArgs e)
        {
         
            FrmFuncionariosConsultar tela = new FrmFuncionariosConsultar();
            CarregarTela();
            panelCentroinicial.Controls.Add(tela);


        }

        private void clienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
           
            FrmClienteConsultar tela = new FrmClienteConsultar();
            CarregarTela();

            panelCentroinicial.Controls.Add(tela);
        }

        private void fornecedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
        
            FrmFornecedorConsultar tela = new FrmFornecedorConsultar();
            CarregarTela();
   
            panelCentroinicial.Controls.Add(tela);
        }

        private void produtosToolStripMenuItem_Click(object sender, EventArgs e)
        {
  
            FrmProdutoConsultar tela = new FrmProdutoConsultar();
            CarregarTela();

            panelCentroinicial.Controls.Add(tela);

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void departamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
      
            FrmDepartamentoCadastro tela = new FrmDepartamentoCadastro();
            CarregarTela();

            panelCentroinicial.Controls.Add(tela);

        }

        private void departamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
            FrmDepartamentoConsultar tela = new FrmDepartamentoConsultar();
            CarregarTela();

            panelCentroinicial.Controls.Add(tela);


        }

        private void TelaInicial_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
       
        private void button1_Click(object sender, EventArgs e)
        {
            FrmUsuarioLogar usuLogin = new FrmUsuarioLogar();
            usuLogin.Show();
            DTOFuncionario dtoEmpty = new DTOFuncionario();

            SessãoUsuario.UsuarioLogado = dtoEmpty;
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DateTime date = DateTime.Now;
            PontoDoDia.PontoDia.Entrada = date;
            btnPonto.Enabled = false;
            btnAlmoco.Enabled = true;
        }
        Timer relogio = new Timer();
        private void button3_Click(object sender, EventArgs e)
        {

            groupBox1.Visible = true;
            consultarToolStripMenuItem.Enabled = false;
            tsmCadastrar.Enabled = false;
            btnAlmoco.Enabled = false;

            
            relogio.Interval = 1000; // 1000 ms = 1s
            decimal tempo = 3600; // 1 Hora

            relogio.Tick += delegate {
                tempo -= 1;
                lblTimer.Text = tempo.ToString();
                label1.Text = "Segundos";
                if (tempo == 0)
                {
                    PararRelogio();
                }
            };
            relogio.Start();
        }

        public void PararRelogio ()
        {
            relogio.Stop();
            MessageBox.Show("Fim do Horario de Almoço", "GG's Person", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            groupBox1.Visible = false;
            consultarToolStripMenuItem.Enabled = true;
            tsmCadastrar.Enabled = true;
            PontoDoDia.PontoDia.Almoco = true;
        }
        private void button4_Click(object sender, EventArgs e)
        {
            PararRelogio();
        }

        private void realizarCompraToolStripMenuItem_Click(object sender, EventArgs e)
        {
       
            FrmCompraCadastro tela = new FrmCompraCadastro();

            CarregarTela();

            panelCentroinicial.Controls.Add(tela);
        }

        private void realizarVendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
     
            FrmVendaCadastro tela = new FrmVendaCadastro();

            CarregarTela();

            panelCentroinicial.Controls.Add(tela);
        }

        private void comprasToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            FrmCompraConsultar tela = new FrmCompraConsultar();

            CarregarTela();

            panelCentroinicial.Controls.Add(tela);
        }

        private void vendasToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            FrmVendaConsultar tela = new FrmVendaConsultar();

            CarregarTela();

            panelCentroinicial.Controls.Add(tela);
        }

        private void gerarFolhaDePagamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
    
            FrmFolhaDePagamento tela = new FrmFolhaDePagamento();

            CarregarTela();

            panelCentroinicial.Controls.Add(tela);
        }

        private void controleDeEstoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGerenciarEstoque tela = new FrmGerenciarEstoque();
            CarregarTela();
            panelCentroinicial.Controls.Add(tela);
        }

        private void fluxoDeCaixaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFluxoDeCaixa tela = new FrmFluxoDeCaixa();
            CarregarTela();
            panelCentroinicial.Controls.Add(tela);
        }

        private void estoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGerenciarEstoque tela = new FrmGerenciarEstoque();
            CarregarTela();
            panelCentroinicial.Controls.Add(tela);
        }
    }
    
}
