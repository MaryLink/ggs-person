﻿namespace PrototioGG_SPERSON.Telas.Consultar
{
    partial class FrmFluxoDeCaixa
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelGerenciarEstoque = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.cboConsultarEstoque = new System.Windows.Forms.ComboBox();
            this.dgvFluxo = new System.Windows.Forms.DataGridView();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelGerenciarEstoque.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFluxo)).BeginInit();
            this.SuspendLayout();
            // 
            // panelGerenciarEstoque
            // 
            this.panelGerenciarEstoque.BackgroundImage = global::PrototioGG_SPERSON.Properties.Resources._586490765_high_resolution_abstract_wallpaper;
            this.panelGerenciarEstoque.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelGerenciarEstoque.Controls.Add(this.button1);
            this.panelGerenciarEstoque.Controls.Add(this.label);
            this.panelGerenciarEstoque.Controls.Add(this.cboConsultarEstoque);
            this.panelGerenciarEstoque.Controls.Add(this.dgvFluxo);
            this.panelGerenciarEstoque.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGerenciarEstoque.Location = new System.Drawing.Point(0, 0);
            this.panelGerenciarEstoque.Name = "panelGerenciarEstoque";
            this.panelGerenciarEstoque.Size = new System.Drawing.Size(963, 515);
            this.panelGerenciarEstoque.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(270, 114);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Consultar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.BackColor = System.Drawing.Color.Transparent;
            this.label.Font = new System.Drawing.Font("Book Antiqua", 27.75F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))));
            this.label.ForeColor = System.Drawing.Color.AliceBlue;
            this.label.Location = new System.Drawing.Point(29, 31);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(267, 45);
            this.label.TabIndex = 5;
            this.label.Text = "Fluxo de Caixa";
            // 
            // cboConsultarEstoque
            // 
            this.cboConsultarEstoque.Font = new System.Drawing.Font("Arial Narrow", 9F);
            this.cboConsultarEstoque.FormattingEnabled = true;
            this.cboConsultarEstoque.Location = new System.Drawing.Point(37, 114);
            this.cboConsultarEstoque.Name = "cboConsultarEstoque";
            this.cboConsultarEstoque.Size = new System.Drawing.Size(228, 24);
            this.cboConsultarEstoque.TabIndex = 4;
            // 
            // dgvFluxo
            // 
            this.dgvFluxo.AllowUserToDeleteRows = false;
            this.dgvFluxo.BackgroundColor = System.Drawing.Color.White;
            this.dgvFluxo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvFluxo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFluxo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Data,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dgvFluxo.GridColor = System.Drawing.Color.Black;
            this.dgvFluxo.Location = new System.Drawing.Point(37, 173);
            this.dgvFluxo.Name = "dgvFluxo";
            this.dgvFluxo.ReadOnly = true;
            this.dgvFluxo.RowHeadersVisible = false;
            this.dgvFluxo.Size = new System.Drawing.Size(866, 278);
            this.dgvFluxo.TabIndex = 3;
            this.dgvFluxo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFluxo_CellContentClick);
            // 
            // Data
            // 
            this.Data.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Data.DataPropertyName = "Data";
            this.Data.HeaderText = "Data";
            this.Data.Name = "Data";
            this.Data.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.DataPropertyName = "Despesas";
            this.Column2.HeaderText = "Despesas";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.DataPropertyName = "Ganhos";
            this.Column3.HeaderText = "Ganhos";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column4.DataPropertyName = "Lucro";
            this.Column4.HeaderText = "Lucro";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // FrmFluxoDeCaixa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelGerenciarEstoque);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmFluxoDeCaixa";
            this.Size = new System.Drawing.Size(963, 515);
            this.panelGerenciarEstoque.ResumeLayout(false);
            this.panelGerenciarEstoque.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFluxo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelGerenciarEstoque;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.ComboBox cboConsultarEstoque;
        private System.Windows.Forms.DataGridView dgvFluxo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
    }
}
