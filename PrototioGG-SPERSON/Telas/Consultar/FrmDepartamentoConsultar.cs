﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Departamento;

namespace PrototioGG_SPERSON.Telas.Consultar
{
    public partial class FrmDepartamentoConsultar : UserControl
    {
        public FrmDepartamentoConsultar()
        {
            InitializeComponent();
        }

        private void btnBuscarDepartamento_Click(object sender, EventArgs e)
        {
            try
            {
                string departamento = txtBuscarDepartamento.Text;
                BusinessDepartamento bus = new BusinessDepartamento();
                List<DTODepartamento> lista = bus.Listar(departamento);

                dgvDepartamento.AutoGenerateColumns = false;
                dgvDepartamento.DataSource = lista;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void dgvDepartamento_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DTODepartamento produto = dgvDepartamento.Rows[e.RowIndex].DataBoundItem as DTODepartamento;
                if (e.ColumnIndex == 02)
                {


                    DialogResult r = MessageBox.Show($"Deseja realmente excluir o departamento {produto.ID}?", "GG's Person",
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        BusinessDepartamento business = new BusinessDepartamento();
                        business.Deletar(produto.ID);

                        btnBuscarDepartamento_Click(null, null);
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
