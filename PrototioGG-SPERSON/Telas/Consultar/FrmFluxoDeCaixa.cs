﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.Classes.Fluxo_de_Caixa;

namespace PrototioGG_SPERSON.Telas.Consultar
{
    public partial class FrmFluxoDeCaixa : UserControl
    {
        public FrmFluxoDeCaixa()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                BusinessFluxoDeCaixa bus = new BusinessFluxoDeCaixa();

                List<DTODespesas> listaDespesas = bus.ConsultarDespesas();
                List<DTOFluxoDeCaixa> listaGanhos = bus.ConsultarGanhos();
                List<FluxoVw> vw = new List<FluxoVw>();

                dgvFluxo.AutoGenerateColumns = false;

                foreach (DTOFluxoDeCaixa item in listaGanhos)
                {

                    foreach (DTODespesas itens in listaDespesas)
                    {
                        if (item.Data.Month == itens.Data.Month && item.Data.Day == itens.Data.Day && item.Data.Year == itens.Data.Year)
                        {
                            FluxoVw list = new FluxoVw();
                            list.Data = item.Data;
                            list.Ganhos = item.Valor;
                            list.Despesas = itens.Valor;
                            list.Lucro = item.Valor - itens.Valor;

                            vw.Add(list);
                        }
                        else
                        {

                        }
                    }
                }

                List<VwFluxo> fluxo = new List<VwFluxo>();

                foreach (FluxoVw item in vw)
                {
                    VwFluxo dto = new VwFluxo();
                    dto.Lucro = item.Lucro;
                    dto.Ganhos = item.Ganhos;
                    dto.Despesas = item.Despesas;
                    dto.Data = item.Data;

                    fluxo.Add(dto);
                }

                foreach (DTODespesas item in listaDespesas)
                {
                    FluxoVw dtooo = new FluxoVw();
                    bool ver = false;
                    bool igg = true;
                    foreach (VwFluxo itens in fluxo)
                    {
                        if (item.Data.Month == itens.Data.Month && item.Data.Day == itens.Data.Day && item.Data.Year == itens.Data.Year)
                        {
                            igg = false;
                        }
                        else
                        {
                            dtooo.Data = item.Data;
                            dtooo.Despesas = item.Valor;
                            dtooo.Ganhos = 0;
                            dtooo.Lucro = item.Valor - item.Valor - item.Valor;

                            ver = true;
                        }
                    }
                    if (ver == true && igg == true)
                    {
                        vw.Add(dtooo);
                    }

                }


                foreach (DTOFluxoDeCaixa item in listaGanhos)
                {
                    FluxoVw dtooo = new FluxoVw();
                    bool ver = false;
                    bool igg = true;
                    foreach (VwFluxo itens in fluxo)
                    {
                        if (item.Data.Month == itens.Data.Month && item.Data.Day == itens.Data.Day && item.Data.Year == itens.Data.Year)
                        {
                            igg = false;
                        }
                        else
                        {
                            dtooo.Data = item.Data;
                            dtooo.Despesas = 0;
                            dtooo.Ganhos = item.Valor;
                            dtooo.Lucro = item.Valor;
                            ver = true;


                        }
                    }
                    if (ver == true && igg == true)
                    {
                        vw.Add(dtooo);
                    }
                }
                dgvFluxo.DataSource = vw;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }

        private void dgvFluxo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
