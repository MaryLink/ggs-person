﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Cliente;
using PrototioGG_SPERSON.Telas.Alterar;

namespace PrototioGG_SPERSON.Telas.Consultar
{
    public partial class FrmClienteConsultar : UserControl
    {
        public FrmClienteConsultar()
        {
            InitializeComponent();
        }

        private void btnBuscarCliente_Click(object sender, EventArgs e)
        {

            string Nome = txtBuscarCliente.Text;
            BusinessCliente bus = new BusinessCliente();
            List<DTOcliente> listar = bus.Consultar(Nome);

            dgvClientes.AutoGenerateColumns = false;
            dgvClientes.DataSource = listar;

        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dgvClientes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DTOcliente cliente = dgvClientes.Rows[e.RowIndex].DataBoundItem as DTOcliente;
                if (e.ColumnIndex == 11)
                {


                    DialogResult r = MessageBox.Show($"Deseja realmente excluir o cliente {cliente.ID}?", "GG's Person",
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        BusinessCliente business = new BusinessCliente();
                        business.Deletar(cliente.ID);

                        btnBuscarCliente_Click(null, null);
                    }
                }
                if (e.ColumnIndex == 10)
                {
                    DTOcliente cliente2 = dgvClientes.CurrentRow.DataBoundItem as DTOcliente;

                    FrmClienteAlterar tela = new FrmClienteAlterar();
                    tela.LoadScreen(cliente2);

                    panelClienteConsultar.Controls.Clear();

                    panelClienteConsultar.Controls.Add(tela);
                }

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "GG's Person",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "GG's Person",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
