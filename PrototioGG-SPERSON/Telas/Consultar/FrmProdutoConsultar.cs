﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Produto;
using PrototioGG_SPERSON.Plugin;
using PrototioGG_SPERSON.Telas.Alterar;

namespace PrototioGG_SPERSON.Telas.Consultar
{
    public partial class FrmProdutoConsultar : UserControl
    {
        public FrmProdutoConsultar()
        {
            InitializeComponent();
        }

        private void btnBuscarProdutos_Click(object sender, EventArgs e)
        {
            try
            {
                string nome = txtBuscarProduto.Text;

                BusinessProduto bus = new BusinessProduto();
                List<DTOProduto> listar = bus.Consultar(nome);

                dgvProdutos.AutoGenerateColumns = false;
                dgvProdutos.DataSource = listar;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }

        private void dgvProdutos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dgvProdutos_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dgvProdutos_CellClick_2(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DTOProduto produto = dgvProdutos.Rows[e.RowIndex].DataBoundItem as DTOProduto;
                if (e.ColumnIndex == 8)
                {


                    DialogResult r = MessageBox.Show($"Deseja realmente excluir o produto {produto.ID}?", "GG's Person",
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        BusinessProduto business = new BusinessProduto();
                        business.Remover(produto.ID);

                        btnBuscarProdutos_Click(null, null);
                    }
                }
                if (e.ColumnIndex == 7)
                {
                    DTOProduto produto2 = dgvProdutos.CurrentRow.DataBoundItem as DTOProduto;

                    FrmProdutoAlterar tela = new FrmProdutoAlterar();
                    tela.LoadScreen(produto2);

                    panelConsultarPRODUTOS.Controls.Clear();

                    panelConsultarPRODUTOS.Controls.Add(tela);
                }
                pictureBox1.Image = ImagemPlugin.ConverterParaImagem(produto.Imagem);

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "GG's Person",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "GG's Person",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
