﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.Classes.Estoque;
using PrototioGG_SPERSON.Classes.Estoque.View;

namespace PrototioGG_SPERSON.Telas.Consultar
{
    public partial class FrmGerenciarEstoque : UserControl
    {

        public FrmGerenciarEstoque()
        {
            InitializeComponent();
        }
        DTOEstoque dt222 = new DTOEstoque();
        private void dgvEstoque_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ViewDTO dt22 = new ViewDTO();
                dt22.Produto = txtBuscarProduto.Text;


                List<ViewDTO> lista = new List<ViewDTO>();

                BusinessView bus = new BusinessView();
                lista = bus.Consultar(dt22);

                dgvEstoque.AutoGenerateColumns = false;
                dgvEstoque.DataSource = lista;
            }

            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }

        private void dgvEstoque_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
    

        }

        private void txtBuscarProduto_TextChanged(object sender, EventArgs e)
        {

        }

        private void panelGerenESTOQUE_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FrmGerenciarEstoque_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

            try
            {
                dt222.FK_Produto = Convert.ToInt32(txtIDPROD.Text);
                dt222.Qtd_Minima = Convert.ToInt32(nudMin.Text);
                dt222.Qtd_Max = Convert.ToInt32(nudMax.Text);

                BusinessEstoque nis = new BusinessEstoque();
                nis.Alterar(dt222);

                ViewDTO dt22 = new ViewDTO();
                dt22.Produto = txtBuscarProduto.Text;


                List<ViewDTO> lista = new List<ViewDTO>();

                BusinessView bus = new BusinessView();
                lista = bus.Consultar(dt22);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void dgvEstoque_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ViewDTO dto = dgvEstoque.CurrentRow.DataBoundItem as ViewDTO;

            txtIDPROD.Text = dto.ID.ToString();

            nudMin.Value = Convert.ToDecimal(dto.Qtd_Min);

            nudMax.Value = Convert.ToDecimal(dto.Qtd_Max);
        }
    }
}
