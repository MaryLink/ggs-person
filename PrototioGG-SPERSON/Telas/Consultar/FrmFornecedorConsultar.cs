﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Fornecedor;

namespace PrototioGG_SPERSON.Telas.Consultar
{
    public partial class FrmFornecedorConsultar : UserControl
    {
        public FrmFornecedorConsultar()
        {
            InitializeComponent();
        }

        private void btnBuscarFornecedor_Click(object sender, EventArgs e)
        {
            string fornecedor = txtBuscarFornecedor.Text;

            BusinessFornecedor bus = new BusinessFornecedor();
            List<DTOFornecedor> listar = bus.Consultar(fornecedor);

            dgvFornecedor.AutoGenerateColumns = false;
            dgvFornecedor.DataSource = listar;
            

            
        }

        private void dgvFornecedor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panelConsultarFORNECEDORES_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnBuscarProdutos_Click(object sender, EventArgs e)
        {
            try
            {
                string fornecedor = txtBuscarFornecedor.Text;

                BusinessFornecedor bus = new BusinessFornecedor();
                List<DTOFornecedor> listar = bus.Consultar(fornecedor);

                dgvFornecedor.AutoGenerateColumns = false;
                dgvFornecedor.DataSource = listar;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void dgvFornecedor_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DTOFornecedor fornecedor = dgvFornecedor.Rows[e.RowIndex].DataBoundItem as DTOFornecedor;
                if (e.ColumnIndex == 10)
                {


                    DialogResult r = MessageBox.Show($"Deseja realmente excluir o Fornecedor {fornecedor.ID}?", "GG's Person",
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        BusinessFornecedor business =  new BusinessFornecedor();
                        business.Deletar(fornecedor.ID);

                        btnBuscarFornecedor_Click(null, null);
                    }
                }
                //if (e.ColumnIndex == 10)
                //{
                //    DTOFornecedor fornecedor2 = dgvFornecedor.CurrentRow.DataBoundItem as DTOFornecedor;

                //    FrmFornecedorAlterar tela = new FrmClienteAlterar();
                //    tela.LoadScreen(fornecedor2);

                //    panelConsultarFORNECEDORES.Controls.Clear();

                //    panelConsultarFORNECEDORES.Controls.Add(tela);
                //}

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "GG's Person",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "GG's Person",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
