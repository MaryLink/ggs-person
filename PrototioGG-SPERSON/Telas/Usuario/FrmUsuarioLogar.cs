﻿using PrototioGG_SPERSON.Classes.Funcionario.BaterPonto;
using PrototioGG_SPERSON.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrototioGG_SPERSON.Telas.Logar
{
    public partial class FrmUsuarioLogar : Form
    {
        public FrmUsuarioLogar()
        {
            InitializeComponent();
            imageOlhoCortado.Visible = false;
            imageOlho1.Visible = true;
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            Efetuar_Login();
        }

        public void Efetuar_Login()
        {
            DTOFuncionario dto = new DTOFuncionario();
            dto.UserName = txtUsername.Text.Trim();
            dto.Password = txtSenha.Text.Trim();

            BusinessFuncionario bus = new BusinessFuncionario();
            dto = bus.Logou(dto);
            if (dto != null)
            {
                SessãoUsuario.UsuarioLogado = dto;

                DTOPonto dtotoos = new DTOPonto();
                PontoDoDia.PontoDia = dtotoos;

                TelaInicial telinha = new TelaInicial();
                telinha.Show();
                this.Hide();

            }
        }

        private void imageOlhoCortado_Click(object sender, EventArgs e)
        {
            imageOlhoCortado.Visible = false;
            imageOlho1.Visible = true;
            txtSenha.UseSystemPasswordChar = true;
        }

        private void imageOlho1_Click(object sender, EventArgs e)
        {
            imageOlho1.Visible = false;
            imageOlhoCortado.Visible = true;
            txtSenha.UseSystemPasswordChar = false;
        }
    }
}
