﻿namespace PrototioGG_SPERSON.Telas.Logar
{
    partial class FrmUsuarioLogar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUsuarioLogar));
            this.label1 = new System.Windows.Forms.Label();
            this.Username = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.btnEntrar = new System.Windows.Forms.Button();
            this.imageOlho1 = new System.Windows.Forms.PictureBox();
            this.imageOlhoCortado = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imageOlho1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageOlhoCortado)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Baskerville Old Face", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gold;
            this.label1.Location = new System.Drawing.Point(360, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "Entrar";
            // 
            // Username
            // 
            this.Username.AutoSize = true;
            this.Username.BackColor = System.Drawing.Color.Transparent;
            this.Username.Font = new System.Drawing.Font("Times New Roman", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Username.ForeColor = System.Drawing.Color.Gold;
            this.Username.Location = new System.Drawing.Point(200, 186);
            this.Username.Name = "Username";
            this.Username.Size = new System.Drawing.Size(90, 26);
            this.Username.TabIndex = 1;
            this.Username.Text = "Usuario:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 17.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gold;
            this.label3.Location = new System.Drawing.Point(215, 245);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Senha:";
            // 
            // txtSenha
            // 
            this.txtSenha.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.txtSenha.Location = new System.Drawing.Point(296, 244);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(232, 26);
            this.txtSenha.TabIndex = 2;
            this.txtSenha.UseSystemPasswordChar = true;
            // 
            // txtUsername
            // 
            this.txtUsername.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsername.Location = new System.Drawing.Point(296, 186);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(232, 26);
            this.txtUsername.TabIndex = 2;
            // 
            // btnEntrar
            // 
            this.btnEntrar.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnEntrar.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnEntrar.FlatAppearance.BorderSize = 3;
            this.btnEntrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEntrar.Font = new System.Drawing.Font("Monotype Corsiva", 13F, System.Drawing.FontStyle.Italic);
            this.btnEntrar.ForeColor = System.Drawing.Color.Gold;
            this.btnEntrar.Location = new System.Drawing.Point(341, 291);
            this.btnEntrar.Name = "btnEntrar";
            this.btnEntrar.Size = new System.Drawing.Size(140, 38);
            this.btnEntrar.TabIndex = 3;
            this.btnEntrar.Text = "Confirmar";
            this.btnEntrar.UseVisualStyleBackColor = false;
            this.btnEntrar.Click += new System.EventHandler(this.btnEntrar_Click);
            // 
            // imageOlho1
            // 
            this.imageOlho1.BackColor = System.Drawing.Color.White;
            this.imageOlho1.BackgroundImage = global::PrototioGG_SPERSON.Properties.Resources.olho2;
            this.imageOlho1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imageOlho1.Location = new System.Drawing.Point(504, 244);
            this.imageOlho1.Name = "imageOlho1";
            this.imageOlho1.Size = new System.Drawing.Size(24, 24);
            this.imageOlho1.TabIndex = 4;
            this.imageOlho1.TabStop = false;
            this.imageOlho1.Click += new System.EventHandler(this.imageOlho1_Click);
            // 
            // imageOlhoCortado
            // 
            this.imageOlhoCortado.BackColor = System.Drawing.Color.White;
            this.imageOlhoCortado.BackgroundImage = global::PrototioGG_SPERSON.Properties.Resources.olho2;
            this.imageOlhoCortado.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imageOlhoCortado.Image = global::PrototioGG_SPERSON.Properties.Resources.olho4;
            this.imageOlhoCortado.Location = new System.Drawing.Point(504, 244);
            this.imageOlhoCortado.Name = "imageOlhoCortado";
            this.imageOlhoCortado.Size = new System.Drawing.Size(24, 24);
            this.imageOlhoCortado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageOlhoCortado.TabIndex = 5;
            this.imageOlhoCortado.TabStop = false;
            this.imageOlhoCortado.Click += new System.EventHandler(this.imageOlhoCortado_Click);
            // 
            // FrmUsuarioLogar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImage = global::PrototioGG_SPERSON.Properties.Resources.Beautiful_Blue_Abstract_Wallpaper;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.imageOlhoCortado);
            this.Controls.Add(this.imageOlho1);
            this.Controls.Add(this.btnEntrar);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Username);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSenha);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmUsuarioLogar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bem Vindo!";
            ((System.ComponentModel.ISupportInitialize)(this.imageOlho1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageOlhoCortado)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Username;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Button btnEntrar;
        private System.Windows.Forms.PictureBox imageOlho1;
        private System.Windows.Forms.PictureBox imageOlhoCortado;
    }
}