﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Produto;
using PrototioGG_SPERSON.Plugin;
using PrototioGG_SPERSON.Telas.Consultar;

namespace PrototioGG_SPERSON.Telas.Alterar
{
    public partial class FrmProdutoAlterar : UserControl
    {
        public FrmProdutoAlterar()
        {
            InitializeComponent();
        }
        
        public void LoadScreen(DTOProduto dto)
        {
            lblID.Text = dto.ID.ToString();
            txtNomeProduto.Text = dto.Nome;
            txtMarcaProduto.Text = dto.Marca;
            txtDescricaoProduto.Text = dto.Descricao;
            txtCategoriaProduto.Text = dto.Categoria;
            nudVlProduto.Value = dto.Valor_Unit_Compra;
            numericUpDown12.Value = dto.Valor_Unit_Venda;
            imgProduto.Image = ImagemPlugin.ConverterParaImagem(dto.Imagem);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DTOProduto dto = new DTOProduto();
                dto.ID = Convert.ToInt32(lblID.Text);
                dto.Nome = txtNomeProduto.Text;
                dto.Marca = txtMarcaProduto.Text;
                dto.Descricao = txtDescricaoProduto.Text;
                dto.Categoria = txtCategoriaProduto.Text;
                dto.Valor_Unit_Compra = nudVlProduto.Value;
                dto.Valor_Unit_Venda = numericUpDown12.Value;
                dto.Imagem = ImagemPlugin.ConverterParaString(imgProduto.Image);

                BusinessProduto bus = new BusinessProduto();
                bus.Alterar(dto);

                MessageBox.Show("Alterado com Sucesso");

                panelProdutoAlterar.Controls.Clear();
                FrmProdutoConsultar frm = new FrmProdutoConsultar();
                panelProdutoAlterar.Controls.Add(frm);

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void imgProduto_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgProduto.ImageLocation = dialog.FileName;
            }
        }
    }
}
