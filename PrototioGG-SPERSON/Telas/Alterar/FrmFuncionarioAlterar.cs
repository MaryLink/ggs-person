﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Funcionario;
using PrototioGG_SPERSON.DB.Departamento;
using PrototioGG_SPERSON.Classes.Funcionario.ViewsFuncionario;

namespace PrototioGG_SPERSON.Telas.Alterar
{
    public partial class FrmFuncionarioAlterar : UserControl
    {
        public FrmFuncionarioAlterar()
        {
            InitializeComponent();
        }

        public void LoadScreen(int id)
        {
            BusinessFuncionario buss = new BusinessFuncionario();
            DTOFuncionario dto = new DTOFuncionario();
            dto = buss.Consultar_ID(id);

            lblID.Text = dto.ID.ToString();
            txtNome.Text = dto.Funcionario;
            txtBairro.Text = dto.Bairro;
            txtCelular.Text = dto.Celular;
            txtCEP.Text = dto.CEP;
            txtCidade.Text = dto.Cidade;
            txtCPF.Text = dto.CPF;
            txtEmail.Text = dto.Email;
            txtEstado.Text = dto.Estado;
            txtRua.Text = dto.Rua;
            txtTelefone.Text = dto.Telefone;
            txtRG.Text = dto.RG;
            txtUsername.Text = dto.UserName;
            txtSenha.Text = dto.Password;
            dateTimePicker1.Value = dto.Nascimento;
           
        }

        private void btnSalvarFORNECEDOR_Click(object sender, EventArgs e)
        {
            try
            {
                DTOFuncionario dto = new DTOFuncionario();
                dto.Bairro = txtBairro.Text.Trim();
                dto.Telefone = txtTelefone.Text.Trim();
                dto.Celular = txtCelular.Text.Trim();
                dto.CEP = txtCEP.Text.Trim();
                dto.Rua = txtRua.Text.Trim();
                dto.Cidade = txtCidade.Text.Trim();
                dto.CPF = txtCPF.Text.Trim();
                dto.RG = txtRG.Text.Trim();
                dto.Email = txtEmail.Text.Trim();
                dto.UserName = txtUsername.Text.Trim();
                dto.Password = txtSenha.Text.Trim();
                dto.Estado = txtEstado.Text.Trim();
                dto.Funcionario = txtNome.Text.Trim();
                dto.Nascimento = dateTimePicker1.Value;
                dto.Entrada = dtpEntrada.Value;
                dto.Saida = dtpSaida.Value;

                DTODepartamento dto2 = new DTODepartamento();
                dto2.Nome = comboBox1.SelectedItem.ToString();

                BusinessFuncionario busf = new BusinessFuncionario();
                busf.Alterar(dto, dto2);
            }

            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


            MessageBox.Show("Alterado com sucesso");
        }
    }
}

