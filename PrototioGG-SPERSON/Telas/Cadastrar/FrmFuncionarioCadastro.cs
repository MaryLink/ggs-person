﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Funcionario;
using PrototioGG_SPERSON.DB.Departamento;
using PrototioGG_SPERSON.Plugin;

namespace PrototioGG_SPERSON.Telas.Cadastrar
{
    public partial class FrmFuncionarioCadastro : UserControl
    {
        public FrmFuncionarioCadastro()
        {
            InitializeComponent();
            BusinessDepartamento bussss = new BusinessDepartamento();
            List<DTODepartamento> list = new List<DTODepartamento>();
            list = bussss.Listar2();
            List<string> liststring = new List<string>();
            foreach (DTODepartamento item in list)
            {
                liststring.Add(item.Nome);
            }
            comboBox1.DataSource = liststring;

        }

        private void btnSalvarFORNECEDOR_Click(object sender, EventArgs e)
        {
            try
            {
                DTOFuncionario dto = new DTOFuncionario();
                dto.Bairro = txtBairro.Text.Trim();
                dto.Telefone = txtTelefone.Text.Trim();
                dto.Celular = txtCelular.Text.Trim();
                dto.CEP = txtCEP.Text.Trim();
                dto.Rua = txtRua.Text.Trim();
                dto.Cidade = txtCidade.Text.Trim();
                dto.CPF = txtCPF.Text.Trim();
                dto.RG = txtRG.Text.Trim();
                dto.Email = txtEmail.Text.Trim();
                dto.UserName = txtUsername.Text.Trim();
                dto.Password = txtSenha.Text.Trim();
                dto.Estado = txtEstado.Text.Trim();
                dto.Funcionario = txtNome.Text.Trim();
                dto.Nascimento = dtpNasc.Value;
                dto.Imagem = ImagemPlugin.ConverterParaString(pictureBox1.Image);
                dto.Entrada = dtpEntrada.Value;
                dto.Saida = dtpSaida.Value;

                DTODepartamento dto2 = new DTODepartamento();
                dto2.Nome = comboBox1.SelectedItem.ToString();

                BusinessFuncionario busf = new BusinessFuncionario();
                int id = busf.Salvar(dto, dto2);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


            MessageBox.Show("Registrado com sucesso");
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
                OpenFileDialog dialog = new OpenFileDialog();
                DialogResult result = dialog.ShowDialog();

                if (result == DialogResult.OK)
                {
                    pictureBox1.ImageLocation = dialog.FileName;
                }

            
        }

        private void panelFuncionarioCadastro_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtCPF_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
