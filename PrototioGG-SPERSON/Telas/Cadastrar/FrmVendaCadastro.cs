﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Cliente;
using PrototioGG_SPERSON.DB.Produto;
using PrototioGG_SPERSON.Classes.Venda.VendaItens;
using PrototioGG_SPERSON.Classes.Venda;
using PrototioGG_SPERSON.Classes.Estoque;
using PrototioGG_SPERSON.Plugin;

namespace PrototioGG_SPERSON.Telas.Cadastrar
{
    public partial class FrmVendaCadastro : UserControl
    {
        public FrmVendaCadastro()
        {
            InitializeComponent();
            carregarProdutos();
            carregarFormPag();
            carregarProdutos();
            CarregarClientes();
            ConfigurarGrid();

        }
        BindingList<DTOProduto> listaprod = new BindingList<DTOProduto>();
        List<DTOsupEstoque> listaEstoque = new List<DTOsupEstoque>();

        void ConfigurarGrid()
        {
            dgvVendaCadastro.AutoGenerateColumns = false;
            dgvVendaCadastro.DataSource = listaprod;
        }
        public void carregarProdutos()
        {
            DTOProduto dto = new DTOProduto();
            BusinessProduto bus = new BusinessProduto();
            List<DTOProduto> lista = bus.listar();
            cboProdutos.ValueMember = nameof(dto.ID);
            cboProdutos.DisplayMember = nameof(dto.Nome);
            cboProdutos.DataSource = lista;

        }

        public void carregarFormPag()
        {
            List<string> lista = new List<string>();
            lista.Add("Debito");
            lista.Add("Credito");
            lista.Add("Dinheiro");

            cboFormaPag.DataSource = lista;

        }

        public void CarregarClientes()
        {
            DTOcliente dto = new DTOcliente();
            BusinessCliente bus = new BusinessCliente();
            List<DTOcliente> lista = bus.Listar();
            cboCliente.ValueMember = nameof(dto.ID);
            cboCliente.DisplayMember = nameof(dto.Nome);
            cboCliente.DataSource = lista;

        }

        decimal total = 0;
        int qtdTotal = 0;


        private void FrmVendaCadastro_Load(object sender, EventArgs e)
        {
            


        }

        private void button1_Click(object sender, EventArgs e)
        {
           

            try
            {
                DTOVenda dto = new DTOVenda();
                dto.Qtd_Produto = qtdTotal;
                dto.Forma_de_Pagamento = cboFormaPag.Text;
                dto.VL_Total = Convert.ToDecimal(lblTotal.Text);
                dto.DT_Venda = DateTime.Now;

                BusinessCliente busCliente = new BusinessCliente();
                DTOcliente dtoCliente = cboCliente.SelectedItem as DTOcliente;
                dto.FK_Cliente = dtoCliente.ID;

                BusinessVenda bus = new BusinessVenda();

                dto.FK_Funcionario = SessãoUsuario.UsuarioLogado.ID;

                bus.Salvar(dto, listaprod.ToList(), listaEstoque.ToList());

                

                MessageBox.Show("Venda confirmada com sucesso");

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            DTOProduto dto = cboProdutos.SelectedItem as DTOProduto;

            int quantidade = Convert.ToInt32(nudQTD.Text);
            decimal preco = 0;

            for (int i = 0; i < quantidade; i++)
            {
                listaprod.Add(dto);
                preco += dto.Valor_Unit_Venda;

            }

            total += preco;
            qtdTotal += quantidade;
            lblTotal.Text = total.ToString();

            DTOsupEstoque dtoEstoque = new DTOsupEstoque();
            dtoEstoque.Nome_Produto = dto.Nome;
            dtoEstoque.Quantidade = quantidade;

            listaEstoque.Add(dtoEstoque);
        }

        private void cboProdutos_SelectedIndexChanged(object sender, EventArgs e)
        {
            DTOProduto dto = cboProdutos.SelectedItem as DTOProduto;

            imgProduto.Image = ImagemPlugin.ConverterParaImagem(dto.Imagem);
        }
    }
}
