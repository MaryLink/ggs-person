﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Fluxo_de_Caixa
{
    class BusinessFluxoDeCaixa
    {
        DatabaseFluxoDeCaixa db = new DatabaseFluxoDeCaixa();

        public List<DTODespesas> ConsultarDespesas()
        {
            return db.ConsultarDespesas();
        }

        public List<DTOFluxoDeCaixa> ConsultarGanhos()
        {
             return db.ConsultarGanhos();

        }

    }
}
