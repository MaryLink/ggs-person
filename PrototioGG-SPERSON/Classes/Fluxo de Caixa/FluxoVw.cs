﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Fluxo_de_Caixa
{
    class FluxoVw
    {

        public decimal Despesas{ get; set; }
        public decimal Ganhos{ get; set; }
        public DateTime Data{ get; set; }
        public decimal Lucro{ get; set; }
    }
}
