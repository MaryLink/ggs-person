﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Departamento
{
    class BusinessDepartamento
    {
        DatabaseDepartamento db = new DatabaseDepartamento();

        public int Salvar(DTODepartamento dto)
        {

            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Infome o nome do Departamento");

            }

           
            if (dto.Salario == 0)
            {
                throw new ArgumentException("Infome o sálario do departamento");

            }

            int pk = db.Salvar(dto);
            return pk;

        }

        public List<DTODepartamento> Listar(string departamento)
        {
            if (departamento == "")
            {
                departamento = string.Empty;

            }

            db.Listar(departamento);
            return db.Listar(departamento);
        }

        public void Deletar(int id)
        {
            db.Deletar(id);
        }

        public void Alterar(DTODepartamento dto)
        {
            db.Alterar(dto);
        }
        public List<DTODepartamento> Listar2()
        {
            return db.Listar2();
        }




    }
}
