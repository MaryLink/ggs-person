﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Produto
{
    class DatabaseProduto
    {
        Database db = new Database();

        public int Salvar(DTOProduto dto)
        {
            string script = @"INSERT INTO tb_produto (nm_produto, ds_produto, ds_categoria, vl_unitario_compra, vl_unitario_venda, ds_marca, imagem)
                                VALUES (@nm_produto, @ds_produto, @ds_categoria, @vl_unitario_compra, @vl_unitario_venda, @ds_marca, @imagem)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("ds_produto", dto.Descricao));
            parms.Add(new MySqlParameter("ds_categoria", dto.Categoria));
            parms.Add(new MySqlParameter("vl_unitario_compra", dto.Valor_Unit_Compra));
            parms.Add(new MySqlParameter("vl_unitario_venda", dto.Valor_Unit_Venda));
            parms.Add(new MySqlParameter("ds_marca", dto.Marca));
            parms.Add(new MySqlParameter("imagem", dto.Imagem));

            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;

        }
        
            public void Deletar(int id)
            {
                string script = @"SET FOREIGN_KEY_CHECKS=0; DELETE FROM tb_produto where id_produto = @id_produto";



                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("id_produto", id));

                db.ExecuteInsertScript(script, parms);
            }

            public void Alterar(DTOProduto dto)
            {
                string script = @"UPDATE tb_produto 
                                     SET nm_produto           = @nm_produto,
                                         vl_unitario_venda    = @vl_unitario_venda,
                                         vl_unitario_compra   = @vl_unitario_compra,
                                         ds_marca             = @ds_marca,
                                         ds_produto           = @ds_produto,
                                         ds_categoria         = @ds_categoria,
                                         imagem               = @imagem
                                   WHERE id_produto = @id_produto";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("id_produto", dto.ID));
                parms.Add(new MySqlParameter("nm_produto", dto.Nome));
                parms.Add(new MySqlParameter("vl_unitario_compra", dto.Valor_Unit_Compra));
                parms.Add(new MySqlParameter("vl_unitario_venda", dto.Valor_Unit_Venda));
                parms.Add(new MySqlParameter("ds_marca", dto.Marca));
                parms.Add(new MySqlParameter("ds_produto", dto.Descricao));
                parms.Add(new MySqlParameter("ds_categoria", dto.Categoria));
                parms.Add(new MySqlParameter("imagem", dto.Imagem));

                Database db = new Database();
                db.ExecuteInsertScript(script, parms);

            }

        public List<DTOProduto> Consultar(string nome)
        {
            string script = @" SELECT * FROM tb_produto 
                                WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", "%" + nome + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTOProduto> lista = new List<DTOProduto>();

            while (reader.Read())
            {
                DTOProduto dto = new DTOProduto();
                dto.ID = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_produto");
                dto.Descricao = reader.GetString("ds_produto");
                dto.Categoria = reader.GetString("ds_categoria");
                dto.Valor_Unit_Compra = reader.GetDecimal("vl_unitario_compra");
                dto.Valor_Unit_Venda = reader.GetDecimal("vl_unitario_venda");
                dto.Marca = reader.GetString("ds_marca");
                dto.Imagem = reader.GetString("imagem");

                lista.Add(dto);
            }

            reader.Close();
            return lista;

        }
        public DTOProduto ConsultarExato(string nome)
        {
            string script = @" SELECT * FROM tb_produto 
                                WHERE nm_produto = @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto",  nome ));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);


            DTOProduto dto = new DTOProduto();

            while (reader.Read())
            {
                dto.ID = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_produto");
                dto.Descricao = reader.GetString("ds_produto");
                dto.Categoria = reader.GetString("ds_categoria");
                dto.Valor_Unit_Compra = reader.GetDecimal("vl_unitario_compra");
                dto.Valor_Unit_Venda = reader.GetDecimal("vl_unitario_venda");
                dto.Marca = reader.GetString("ds_marca");
              
            }

            reader.Close();
            return dto;

        }

            public List<DTOProduto> Listar()
        {
            string script = @" SELECT * FROM tb_produto";
         
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<DTOProduto> lista = new List<DTOProduto>();

            while (reader.Read())
            {
                DTOProduto dto = new DTOProduto();
                dto.ID = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_produto");
                dto.Descricao = reader.GetString("ds_produto");
                dto.Categoria = reader.GetString("ds_categoria");
                dto.Valor_Unit_Compra = reader.GetDecimal("vl_unitario_compra");
                dto.Valor_Unit_Venda = reader.GetDecimal("vl_unitario_venda");
                dto.Marca = reader.GetString("ds_marca");
                dto.Imagem = reader.GetString("imagem");

                lista.Add(dto);
            }

            reader.Close();
            return lista;

        }




    }
}
