﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Estoque
{
    class DTOEntrada
    {
        public int ID { get; set; }
        public DateTime Data_Entrega { get; set; }
        public int FK_Compra { get; set; }
        public int FK_Estoque { get; set; }
    }
}
