﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Estoque
{
    class DatabaseEstoque
    {
        public DTOEstoque Consultar(DTOEstoque dto)
        {
            string script = @"SELECT * FROM tb_estoque WHERE fk_produto = @fk_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_produto", dto.FK_Produto));

            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if (reader.Read())
            {
                dto.ID = reader.GetInt32("id_estoque");
                dto.Qtd_Atual = reader.GetInt32("qtd_atual");
                dto.Qtd_Max = reader.GetInt32("qtd_max");
                dto.Qtd_Minima = reader.GetInt32("qtd_min");
            }
            else
            {
                dto = null;
            }

            reader.Close();

            return dto;
        }
        public void Alterar(DTOEstoque dto)
        {
            string script = @"UPDATE tb_estoque SET qtd_min = @qtd_min, 
                                                    qtd_max = @qtd_max     
                                              WHERE fk_produto = @fk_produto";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_produto", dto.FK_Produto));
            parms.Add(new MySqlParameter("qtd_min", dto.Qtd_Minima));
            parms.Add(new MySqlParameter("qtd_max", dto.Qtd_Max));

            Database db = new Database();

            db.ExecuteInsertScript(script, parms);

            


        }

       
    }
}
