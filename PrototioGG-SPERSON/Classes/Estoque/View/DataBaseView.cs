﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Estoque.View
{
    class DataBaseView
    {
        Database db = new Database();
        public List<ViewDTO> Consultar(ViewDTO dto)
        {
            string script = @"SELECT * FROM vw_estoque WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto","%" + dto.Produto + "%"));



            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ViewDTO> lista = new List<ViewDTO>();

            while (reader.Read())
            {
                ViewDTO dto2 = new ViewDTO();

                dto2.ID = reader.GetInt32("id_produto");
                dto2.Produto = reader.GetString("nm_produto");
                dto2.Qtd_Atual = reader.GetInt32("qtd_atual");
                dto2.Qtd_Max = reader.GetInt32("qtd_max");
                dto2.Qtd_Min = reader.GetInt32("qtd_min");

                lista.Add(dto2);
            }
            reader.Close();

            return lista;
        }

    }

}
