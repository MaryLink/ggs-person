﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Estoque.Saida
{
    class DTOSaida
    {
        public int ID { get; set; }

        public DateTime Data_Entrega { get; set; }

        public int FK_Venda { get; set; }

        public int FK_Estoque { get; set; }
    }
}
