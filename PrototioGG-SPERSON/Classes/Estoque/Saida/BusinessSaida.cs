﻿using PrototioGG_SPERSON.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Estoque.Saida
{
    class BusinessSaida
    {
        
            DatabaseSaida db = new DatabaseSaida();


        public void AtualizarEstoque(int qtd_comprada, string nome_produto)
        {
            BusinessEstoque bussEs = new BusinessEstoque();
            DTOEstoque dto = new DTOEstoque();
            DTOEstoque dto2 = new DTOEstoque();
            BusinessProduto bussProd = new BusinessProduto();
            DTOProduto dtoProd = new DTOProduto();

            dtoProd = bussProd.ConsultarExato(nome_produto);

            dto.FK_Produto = dtoProd.ID;

            dto2 = bussEs.Consultar(dto);
            if (dto2 == null)
            {
                dto.Qtd_Atual = 0;
                dto.Qtd_Max = 0;
                dto.Qtd_Minima = 0;

                db.SalvarEstoque(dto);

                dto2 = bussEs.Consultar(dto);
            }

            db.AtualizarEstoque(dto2.Qtd_Atual, qtd_comprada, dtoProd.ID);


        }
    }
}
