﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Estoque.Saida
{
    class DatabaseSaida
    {
        public void AtualizarEstoque(int qtd_atual,int qtd_vendida, int id_produto)
        {
            string script = @"UPDATE tb_estoque
                              SET qtd_atual = @qtd_atual - @qtd_vendida
                              WHERE fk_produto = @fk_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qtd_atual", qtd_atual));
            parms.Add(new MySqlParameter("qtd_vendida", qtd_vendida));
            parms.Add(new MySqlParameter("fk_produto", id_produto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public int SalvarEstoque(DTOEstoque dto)
        {
            string script = @"INSERT tb_estoque(qtd_atual, fk_produto)Values (@qtd_atual, @fk_produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qtd_atual", dto.Qtd_Atual));
            parms.Add(new MySqlParameter("fk_produto", dto.FK_Produto));

            Database db = new Database();
            int id = db.ExecuteInsertScriptWithPk(script, parms);

            return id;
        }
    }
}
