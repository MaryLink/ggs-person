﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Estoque
{
    class DTOEstoque
    {
        public int ID { get; set; }

        public int Qtd_Atual{ get; set; }

        public int Qtd_Minima{ get; set; }

        public int Qtd_Max { get; set; }

        public int FK_Produto { get; set; }
    }
}
