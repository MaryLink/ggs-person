﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Compra.CompraItens
{
    class DTOCompraItem
    {
        public int ID { get; set; }

        public int FK_Produto { get; set; }

        public int FK_Compra{ get; set; }
    }
}
