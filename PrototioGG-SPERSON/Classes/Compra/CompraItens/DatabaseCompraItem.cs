﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Compra.CompraItens
{
    class DatabaseCompraItem
    {
        public int Salvar(DTOCompraItem dto)
        {
            string script = @"INSERT INTO tb_compra_item (fk_produto, fk_compra) VALUES (@fk_produto, @fk_compra)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_produto", dto.FK_Produto));
            parms.Add(new MySqlParameter("fk_compra", dto.FK_Compra));

            Database db = new Database();
            int id = db.ExecuteInsertScriptWithPk(script, parms);
            
            return id;
        }
    }
}
