﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Compra
{
    class DTOCompra
    {
        public int ID { get; set; }

        public string Forma_De_Pagamento { get; set; }

        public decimal VL_Total { get; set; }

        public DateTime DT_Compra{ get; set; }

        public int Qtd_Produtos { get; set; }

        public int FK_Fornecedor { get; set; }
    }
}
