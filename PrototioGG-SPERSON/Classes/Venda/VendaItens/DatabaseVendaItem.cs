﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Venda.VendaItens
{
    class DatabaseVendaItem
    {
        public int Salvar(DTOVendaItem dto)
        {
            string script = @"INSERT INTO tb_venda_item (fk_produto, fk_venda) VALUES (@fk_produto, @fk_venda)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_produto", dto.FK_Produto));
            parms.Add(new MySqlParameter("fk_venda", dto.FK_Venda));

            Database db = new Database();
            int id = db.ExecuteInsertScriptWithPk(script, parms);

            return id;
        }

    }
}
