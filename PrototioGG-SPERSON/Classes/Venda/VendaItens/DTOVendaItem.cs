﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Venda.VendaItens
{
    class DTOVendaItem
    {
        public int ID { get; set; }

        public int FK_Produto { get; set; }

        public int FK_Venda { get; set; }

    }
}
