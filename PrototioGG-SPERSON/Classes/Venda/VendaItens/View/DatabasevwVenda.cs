﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Venda.VendaItens.View
{
    class DatabasevwVenda
    {
        public List<DTOvwVenda> ConsultarView(DTOvwVenda dto)
        {
            string script = @"SELECT * FROM   vw_consultar_venda
                                       WHERE  nm_produto   
                                        LIKE @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", "%" + dto.nm_produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTOvwVenda> dto3 = new List<DTOvwVenda>();
            while (reader.Read())
            {
                DTOvwVenda dto2 = new DTOvwVenda();
                dto2.id_venda = reader.GetInt32("id_venda");
                dto2.nm_produto = reader.GetString("nm_produto");
                dto2.vl_unitario_venda = reader.GetDecimal("vl_unitario_venda");
                dto2.qtd_produto = reader.GetInt32("qtd_produto");
                dto2.forma_de_pagamento = reader.GetString("forma_de_pagamento");
                dto2.dt_venda = reader.GetDateTime("dt_venda");
                dto2.vl_total = reader.GetDecimal("vl_total");
                dto2.nm_funcionario = reader.GetString("nm_funcionario");
                dto2.nm_cliente = reader.GetString("nm_cliente");
               

                dto3.Add(dto2);
            }
            reader.Close();
            return dto3;
        }
        
            public List<DTOvwVenda> ConsultarViewFunc(DTOvwVenda dto)
            {
                string script = @"SELECT * FROM   vw_consultar_venda
                                       WHERE  nm_funcionario
                                        = @nm_funcionario";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("nm_funcionario", dto.nm_funcionario));

                Database db = new Database();
                MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

                List<DTOvwVenda> dto3 = new List<DTOvwVenda>();
                while (reader.Read())
                {
                    DTOvwVenda dto2 = new DTOvwVenda();
                    dto2.id_venda = reader.GetInt32("id_venda");
                    dto2.nm_produto = reader.GetString("nm_produto");
                    dto2.vl_unitario_venda = reader.GetDecimal("vl_unitario_venda");
                    dto2.qtd_produto = reader.GetInt32("qtd_produto");
                    dto2.forma_de_pagamento = reader.GetString("forma_de_pagamento");
                    dto2.dt_venda = reader.GetDateTime("dt_venda");
                    dto2.vl_total = reader.GetDecimal("vl_total");
                    dto2.nm_funcionario = reader.GetString("nm_funcionario");
                    dto2.nm_cliente = reader.GetString("nm_cliente");


                    dto3.Add(dto2);
                }
                reader.Close();
                return dto3;
            }












        }
}
