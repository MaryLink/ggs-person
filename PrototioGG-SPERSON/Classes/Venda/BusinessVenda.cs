﻿using PrototioGG_SPERSON.Classes.Estoque;
using PrototioGG_SPERSON.Classes.Estoque.Saida;
using PrototioGG_SPERSON.Classes.Venda.VendaItens;
using PrototioGG_SPERSON.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Venda
{
    class BusinessVenda
    {
        public int Salvar(DTOVenda venda, List<DTOProduto> item, List<DTOsupEstoque> estoque)
        {
            DatabaseVenda vendaDatabase = new DatabaseVenda();
            int idVenda = vendaDatabase.Salvar(venda);

            BusinessSaida buss = new BusinessSaida();

            BusinessVendaItem business = new BusinessVendaItem();
            foreach (DTOProduto itens in item)
            {
                DTOVendaItem itemDto = new DTOVendaItem();
                itemDto.FK_Venda = idVenda;
                itemDto.FK_Produto = itens.ID;

                business.Salvar_FKs(itemDto);
            }

            foreach (DTOsupEstoque itens in estoque)
            {
                buss.AtualizarEstoque(itens.Quantidade, itens.Nome_Produto);
            }

            return idVenda;
        }
    }
}
