﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Venda
{
    class DTOVenda
    {
        public int ID { get; set; }

        public string Forma_de_Pagamento { get; set; }

        public decimal VL_Total{ get; set; }

        public DateTime DT_Venda { get; set; }

        public int Qtd_Produto { get; set; }

        public int FK_Cliente { get; set; }

        public int FK_Funcionario{ get; set; }
    }
}
