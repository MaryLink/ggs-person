﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Venda
{
    class DatabaseVenda
    {
        public int Salvar(DTOVenda dto)
        {
            string script = @"INSERT INTO tb_venda (forma_de_pagamento, vl_total, dt_venda, qtd_produto, fk_cliente, fk_funcionario)
                                VALUES (@forma_de_pagamento, @vl_total, @dt_venda, @qtd_produto, @fk_cliente, @fk_funcionario)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("forma_de_pagamento", dto.Forma_de_Pagamento));
            parms.Add(new MySqlParameter("dt_venda", dto.DT_Venda));
            parms.Add(new MySqlParameter("qtd_produto", dto.Qtd_Produto));
            parms.Add(new MySqlParameter("vl_total", dto.VL_Total));
            parms.Add(new MySqlParameter("fk_funcionario", dto.FK_Funcionario));
            parms.Add(new MySqlParameter("fk_cliente", dto.FK_Cliente));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_venda WHERE id_venda = @id_venda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_venda", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

     

    }
}
