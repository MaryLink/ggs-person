﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento.INSS
{
    class DTOInss
    {
        public int Id_Inss { get; set; }
        public decimal Valor_Faixa_Salario { get; set; }
        public decimal Valor_Aliquota { get; set; }

    }
}
