﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento.INSS
{
    class CalcularINSS
    {
        public decimal SaldoINSS (decimal salariobruto)
        {
            decimal valorINSS = 0;
                if (salariobruto <= 1693.73m)
                {
                valorINSS  = salariobruto * 0.08m;
                }
                else if (salariobruto > 1693.73m && salariobruto< 2822.91m)
                {
                valorINSS = salariobruto* 0.09m;
                }
                else if (salariobruto >= 2822.91m)
                {
                valorINSS = salariobruto* 0.11m;
                }

            return valorINSS;
        }

      
    }
}
