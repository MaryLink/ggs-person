﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento.IR
{
    class CalculoIR
    {
        public decimal IR(decimal salariobruto)
        {
            decimal ir = 0;
            decimal deducaoIR = 0;
            decimal valorIR = 0;

            //IR - coluna de aliquota 
            if (salariobruto == 0)
            {
                ir = salariobruto * 0;
                deducaoIR = 0;
                // Valor ir 
                valorIR = ir - deducaoIR;

            }
            else if (salariobruto <= 1903.98m && salariobruto > 0)
            {
                ir = salariobruto * 0.075m;
                deducaoIR = 142.80m;
                // Valor ir 
                valorIR = ir - deducaoIR;

            }
            else if (salariobruto <= 2826.65m && salariobruto > 1903.98m)
            {
                ir = salariobruto * 0.15m;
                deducaoIR = 354.80m;
                // Valor ir 
                valorIR = ir - deducaoIR;

            }
            else if (salariobruto <= 3751.05m && salariobruto > 2826.65m)
            {
                ir = salariobruto * 0.02250m;
                deducaoIR = 636.13m;
                // Valor ir 
                valorIR = ir - deducaoIR;

            }
            else if (salariobruto <= 4664.68m && salariobruto > 3751.05m)
            {
                ir = salariobruto * 0.275m;
                deducaoIR = 869.36m;
                // Valor ir 
                valorIR = ir - deducaoIR;

            }

            return valorIR;
        }

    }
}
