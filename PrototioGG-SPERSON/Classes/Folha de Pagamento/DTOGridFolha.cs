﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento
{
    class DTOGridFolha
    {
        public string Funcionario { get; set; }

        public decimal Salario_Liquido { get; set; }

        public decimal IR{ get; set; }

        public decimal INSS{ get; set; }

        public decimal Salario_Bruto{ get; set; }

        public decimal Comissao_Vendas{ get; set; }

        public decimal VT{ get; set; }

        public decimal VR{ get; set; }
        //hora extra
        public decimal HX{ get; set; }
    }
}
