﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento
{
    class CalcularFGTS
    {
        public decimal CalculoFGTS(decimal salarioBruto)
        {
            decimal ValorFGTS = salarioBruto * 0.08m;
            return ValorFGTS;

        }

        public decimal CalcularValeRefeicao(decimal salarioBruto)
        {
         decimal ValorValeRefeicao = salarioBruto * 0.08m;
            return ValorValeRefeicao;
        }

        public decimal CalcularValeTransporte(decimal salarioBruto)
        {
            decimal ValorValeTransposte = salarioBruto * 0.06m;
            return ValorValeTransposte;
        }
        



    }
}
