﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento
{
    class DTOFolhaDePagamento
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Funcao { get; set; }
        public decimal SálarioBruto { get; set; }
        public int TotalDiasMes { get; set; }
        public decimal ValorMes { get; set; }
        public decimal ValorHora { get; set; }
        public decimal ValorHora50 { get; set; }
        public decimal ValorHora100 { get; set; }
        public int HorasTrabalhadas50 { get; set; }
        public decimal Valor2Horas50 { get; set; }
        public int  HorasTrabalhadas100 { get; set; }
        public decimal ValorTrabalho100 { get; set; }
        public decimal ValorINSS { get; set; }
        public decimal Imposto_Renda { get; set; }
        public decimal Deducao_ir { get; set; }
        public decimal Valor_IR { get; set; }
        public decimal Valor_FGTS { get; set; }
        public decimal Valor_VR { get; set; }
        public decimal Valor_VT { get; set; }
        public decimal Total_Proventos { get; set; }
        public decimal Total_Descontos { get; set; }
        public decimal Salario_Liquido { get; set; }
        public decimal Salario { get; set; }
        public decimal Aliquota { get; set; }
        public decimal Deduzir { get; set; }


    }
}
