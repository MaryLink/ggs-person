﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento.Descontos
{
    class DTOdescontos
    {
        public int Id_Descontos { get; set; }
        public decimal Valor_Vale_Transporte { get; set; }
        public decimal Valor_Vale_Refeicao { get; set; }
        public decimal Valor_FGTS { get; set; }
        public decimal TotalDesconto { get; set; }

    }
}
