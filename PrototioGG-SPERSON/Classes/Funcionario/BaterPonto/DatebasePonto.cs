﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Funcionario.BaterPonto
{
    class DatebasePonto
    {
   
        public int Salvar(DTOPonto dto)
        {
            string script = @"INSERT INTO tb_ponto (dt_ponto, dt_atraso, hr_extra, hr_jornada_trabalho_inicio, hr_jornada_trabalho_fim, bl_almoco, fk_funcionario) VALUES (@dt_ponto, @dt_atraso, @hr_extra, @hr_jornada_trabalho_inicio, @hr_jornada_trabalho_fim, @bl_almoco,@fk_funcionario)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_ponto", dto.Data));
            parms.Add(new MySqlParameter("dt_atraso", dto.Atraso));
            parms.Add(new MySqlParameter("hr_extra", dto.Extra));
            parms.Add(new MySqlParameter("hr_jornada_trabalho_inicio", dto.Entrada));
            parms.Add(new MySqlParameter("hr_jornada_trabalho_fim", dto.Saida));
            parms.Add(new MySqlParameter("bl_almoco", dto.Almoco));
            parms.Add(new MySqlParameter("fk_funcionario", dto.FK_Funcionario));

            Database db = new Database();
            int id = db.ExecuteInsertScriptWithPk(script, parms);

            return id;
        }
    }
}
