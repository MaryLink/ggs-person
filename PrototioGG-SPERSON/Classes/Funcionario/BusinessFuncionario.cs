﻿
using PrototioGG_SPERSON.DB.Departamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Funcionario
{
    class BusinessFuncionario
    {
        DatabaseDepartamento dbb = new DatabaseDepartamento();
        DatabaseFuncionario db = new DatabaseFuncionario();
        public int Salvar(DTOFuncionario dto, DTODepartamento dto2)
        {
            List<DTODepartamento> dtooos = new List<DTODepartamento>();

            dtooos = dbb.Listar(dto2.Nome);
            foreach (DTODepartamento item in dtooos)
            {
                dto2.ID = item.ID;
            }
            dto.FK_Departamento = dto2.ID;

            if (dto.Funcionario == string.Empty)
            {
                throw new ArgumentException("Infome o nome do Funcionario");

            }
            
            
            if (dto.Email == string.Empty)
            {
                throw new ArgumentException("Infome o email do Funcionario");

            }


            if (dto.Celular == string.Empty)
            {
                throw new ArgumentException("Infome o celular do Funcionario");

            }


            if (dto.RG == string.Empty)
            {
                throw new ArgumentException("Infome o RG do Funcionario");

            }


            if (dto.CEP == string.Empty)
            {
                throw new ArgumentException("Infome o CEP do Funcionario");

            }


            if (dto.Celular == string.Empty)
            {
                throw new ArgumentException("Infome o Celular do Funcionario");

            }

            DateTime MenorDeIdade = Convert.ToDateTime("2002/01/01");
            bool dataInvalida = (dto.Nascimento >= MenorDeIdade);

            if (dataInvalida == true)
            {
                throw new ArgumentException("A idade do funcionario é invalida, menor que 14anos");
            }


            if (dto.Rua == string.Empty)
            {
                throw new ArgumentException("Infome a rua do Funcionario");

            }


            if (dto.Password == string.Empty)
            {
                throw new ArgumentException("Infome a password da usuario do Funcionario");

            }

            if (dto.Imagem == string.Empty)
            {
                throw new ArgumentException("Coloque uma Imagem de Perfil");

            }



            if (dto.UserName == string.Empty)
            {
                throw new ArgumentException("Infome o username do Funcionario");

            }


            if (dto.Bairro == string.Empty)
            {
                throw new ArgumentException("Infome Bairro do Funcionario");

            }


            if (dto.CPF == string.Empty)
            {
                throw new ArgumentException("Infome o CPF do Funcionario");

            }

            int id = db.Salvar(dto);
            return id;
        }

        public DTOFuncionario Logou(DTOFuncionario dto)
        {
            dto = db.Logar(dto);

            return dto;
        }

        public void Remover(int id)
        {
            db.Deletar(id);
        }

        public void Alterar(DTOFuncionario dto, DTODepartamento dto2)
        {
            List<DTODepartamento> list = dbb.Listar(dto2.Nome);
            foreach (DTODepartamento item in list)
            {
                dto.FK_Departamento = item.ID;
            }
            db.Alterar(dto);
        }

        public List<DTOFuncionario> Consultar(String funcionario)
        {
            if (funcionario == "")
            {
                funcionario = string.Empty;

            }
            List<DTOFuncionario> dto = new List<DTOFuncionario>();

            dto = db.Consultar(funcionario);

            return dto;

        }

        public DTOFuncionario Consultar_ID(int ID)
        {

            DTOFuncionario dto = new DTOFuncionario();

            dto = db.Consultar_ID(ID);

            return dto;

        }

        
    }
}
